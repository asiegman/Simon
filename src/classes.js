export class Reaction {
    constructor(filename, display, type) {
        this.display = display;
        this.type = type;
        this.filename = filename;
    }

    matches(filter, type) {
        if (filter && !this.display.toLowerCase().includes(filter.toLowerCase())) {
            return false;
        }

        if (type && this.type !== type) {
            return false;
        }

        return true;
    }

    getMark() {
        return Reaction.TYPES[this.type];
    }

    static get TYPES() {
        return {
            Positive: 'success',
            Neutral: 'warning',
            Negative: 'danger',
        }
    }
}
