import { Reaction } from "./classes";

export const reactions = [
    new Reaction('Aaah.mp3', 'Aaah!', 'Positive'),
    new Reaction('Ah now hang on.mp3', 'Ah! Now hang on!', 'Positive'),
    new Reaction('Oh dear.mp3', 'Oh dear!', 'Positive'),
    new Reaction('This is fascinating.mp3', 'This is fascinating!', 'Positive'),
    new Reaction('What a puzzle this is.mp3', 'What a puzzle this is!', 'Positive'),
    new Reaction('My goodness me.mp3', 'My goodness me!', 'Positive'),
    new Reaction('Loved it.mp3', 'Loved it!', 'Positive'),
    new Reaction('It\'s really lovely.mp3', 'It\'s really lovely', 'Positive'),
    new Reaction('That is absolutely lovely.mp3', 'That is absolutely lovely!', 'Positive'),
    new Reaction('Masterpieces.mp3', 'Masterpieces!', 'Positive'),
    new Reaction('What a brilliant puzzle.mp3', 'What a brilliant puzzle!', 'Positive'),
    new Reaction('This is great.mp3', 'This is great!', 'Positive'),
    new Reaction('Well done, me!.mp3', 'Well done, me!', 'Positive'),
    new Reaction('Ah! That\'s interesting.mp3', 'Ah! That\'s interesting', 'Positive'),
    new Reaction('Holy Molly!.mp3', 'Holy Molly!', 'Positive'),
    new Reaction('Oh!.mp3', 'Oh!', 'Positive'),
    new Reaction('Oh, oh, look.mp3', 'Oh, oh, look!', 'Positive'),
    new Reaction('Wow!.mp3', 'Wow!', 'Positive'),
    new Reaction('This is a work of sublime genius.mp3', 'This is a work of sublime genius!', 'Positive'),
    new Reaction('This is very, very nice.mp3', 'This is very, very nice', 'Positive'),
    new Reaction('Ah, now, now I have, ah, yes yes, yes.mp3', 'Ah, now, now I have, ah, yes yes, yes', 'Positive'),
    new Reaction('Straight off the bat, we are up and running.mp3', 'Straight off the bat, we are up and running', 'Positive'),
    new Reaction('This is it.mp3', 'This is it!', 'Positive'),
    new Reaction('This is lovely geometry.mp3', 'This is lovely geometry', 'Positive'),
    new Reaction('This is one of the greatest puzzles.mp3', 'This is one of the greatest puzzles!', 'Positive'),
    new Reaction('This is one of the most beautiful executions.mp3', 'This is one of the most beautiful executions!', 'Positive'),
    new Reaction('This is unbelievable.mp3', 'This is unbelievable!', 'Positive'),
    new Reaction('This might be the best solve I have ever done on this channel!.mp3', 'The best solve I have ever done on this channel!', 'Positive'),
    new Reaction('Woah moment.mp3', 'Woah moment!', 'Positive'),
    new Reaction('Yes, this!.mp3', 'Yes, this!', 'Positive'),
    new Reaction('Yeah, 69!.mp3', 'Yeah, 69!', 'Positive'),

    new Reaction('This is a good idea.mp3', 'This is a good idea', 'Positive'),
    new Reaction('That was a good idea.mp3', 'That was a good idea', 'Negative'),
    new Reaction('I have no idea.mp3', 'I have no idea', 'Negative'),
    new Reaction('Oh, no, I\'ve had an idea.mp3', 'Oh, no, I\'ve had an idea!', 'Positive'),

    new Reaction('Hello and welcome.mp3', 'Hello and welcome!', 'Neutral'),
    new Reaction('Let\'s get cracking.mp3', 'Let\'s get cracking', 'Neutral'),
    new Reaction('Now.mp3', 'Now…', 'Neutral'),
    new Reaction('Again.mp3', 'Again…', 'Neutral'),
    new Reaction('Things you hear on Cracking The Cryptic.mp3', 'Things you hear on Cracking The Cryptic', 'Neutral'),
    new Reaction('Knowledge bomb from Cracking The Cryptic.mp3', 'Knowledge bomb from Cracking The Cryptic', 'Neutral'),
    new Reaction('Hang on, just a moment.mp3', 'Hang on, just a moment…', 'Neutral'),
    new Reaction('Naked single.mp3', 'Naked single', 'Neutral'),
    new Reaction('Spotting naked singles.mp3', 'Spotting naked singles', 'Neutral'),
    new Reaction('Is that a naked single.mp3', 'Is that a naked single?', 'Neutral'),
    new Reaction('Please let it be a naked single.mp3', 'Please let it be a naked single!', 'Neutral'),
    new Reaction('Normal sudoku rules apply.mp3', 'Normal sudoku rules apply', 'Neutral'),
    new Reaction('Very amusing.mp3', 'Very amusing', 'Neutral'),
    new Reaction('We\'re gonna have to do some trickery.mp3', 'We\'re gonna have to do some trickery', 'Neutral'),
    new Reaction('I\'ve got nothing else to do. Let\'s do it.mp3', 'I\'ve got nothing else to do. Let\'s do it', 'Neutral'),
    new Reaction('I don\'t believe it.mp3', 'I don\'t believe it', 'Neutral'),
    new Reaction('Surely I can disambiguate this.mp3', 'Surely I can disambiguate this', 'Neutral'),
    new Reaction('Because.mp3', 'Because', 'Neutral'),
    new Reaction('It\'s probably on the bingo card.mp3', 'It\'s probably on the bingo card', 'Neutral'),
    new Reaction('So.mp3', 'So…', 'Neutral'),
    new Reaction('So where shall we look.mp3', 'So where shall we look?', 'Neutral'),
    new Reaction('This is all stripey.mp3', 'This is all stripey', 'Neutral'),
    new Reaction('Watch this.mp3', 'Watch this!', 'Neutral'),

    new Reaction('Bother.mp3', 'Bother', 'Negative'),
    new Reaction('Bobbins.mp3', 'Bobbins', 'Negative'),
    new Reaction('Archbobbins!.mp3', 'Archbobbins!', 'Negative'),
    new Reaction('Shenanigans.mp3', 'Shenanigans', 'Negative'),
    new Reaction('Now that is worth a Bobbins.mp3', 'Now that is worth a Bobbins', 'Negative'),
    new Reaction('That is absolutely hopeless.mp3', 'That is absolutely hopeless', 'Negative'),
    new Reaction('And that is absolutely useless.mp3', 'And that… is absolutely useless', 'Negative'),
    new Reaction('Now that, of course, is completely useless.mp3', 'Now that, of course, is completely useless', 'Negative'),
    new Reaction('Why did I say that.mp3', 'Why did I say that?', 'Negative'),
    new Reaction('It\'s because I am a moron.mp3', 'It\'s because I am a moron', 'Negative'),
    new Reaction('No, bobbins, this is nonsense.mp3', 'No, bobbins, this is nonsense.', 'Negative'),
    new Reaction('Shouldn\'t\'ve said that.mp3', 'Shouldn\'t\'ve said that…', 'Negative'),
    new Reaction('Good grief.mp3', 'Good grief', 'Negative'),
    new Reaction('The dreaded bifurcation.mp3', 'The dreaded bifurcation', 'Negative'),
    new Reaction('Ah! No good, it\'s no good.mp3', 'Ah! No good, it\'s no good', 'Negative'),
    new Reaction('This is madness.mp3', 'This is madness', 'Negative'),
    new Reaction('I could have done it for a while.mp3', 'I could have done it for a while', 'Negative'),
    new Reaction('I\'m still miles away from finishing it.mp3', 'I\'m still miles away from finishing it', 'Negative'),
    new Reaction('I don\'t have a clue what I am meant to do now.mp3', 'I don\'t have a clue what I am meant to do now', 'Negative'),
    new Reaction('Punched in the face repeatedly.mp3', 'Punched in the face repeatedly', 'Negative'),
    new Reaction('I haven\'t go a scooby-doo!.mp3', 'I haven\'t go a scooby-doo!', 'Negative'),
    new Reaction('I think I\'m going mad.mp3', 'I think I\'m going mad', 'Negative'),
    new Reaction('I\'ve got one digit in the grid.mp3', 'I\'ve got one digit in the grid!', 'Negative'),
    new Reaction('Now I can\'t see the woods through the trees at all.mp3', 'Now I can\'t see the woods through the trees at all', 'Negative'),
    new Reaction('Sorry, I am going to stare at this a little bit longer.mp3', 'I am going to stare at this a little bit longer', 'Negative'),
    new Reaction('What do we do now.mp3', 'What do we do now?', 'Negative'),
    new Reaction('What does this mean.mp3', 'What does this mean?', 'Negative'),
    new Reaction('What on Earth am I meant to do with this.mp3', 'What on Earth am I meant to do with this?', 'Negative'),
    new Reaction('This is mindbendingly complicated.mp3', 'This is mindbendingly complicated', 'Negative'),
    new Reaction('It\'s beautiful and useless.mp3', 'It\'s beautiful and useless', 'Negative'),
];
